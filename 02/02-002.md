### Linux 初始化调整

```shell
andre@ubuntu1:~$ sudo passwd root
andre@ubuntu1:~$ su
Password: 
root@ubuntu1:/home/andre# vi /etc/ssh/sshd_config

修改权限
# Authentication:
LoginGraceTime 120
#PermitRootLogin prohibit-password
PermitRootLogin yes
StrictModes yes

重启服务
root@ubuntu2:/home/andre# service ssh restart


root账号切换
root@ubuntu1:~# su andre
andre@ubuntu1:/root$ ll
ls: cannot open directory '.': Permission denied
andre@ubuntu1:/root$ su
Password: 
su: Authentication failure
andre@ubuntu1:/root$ cd/root/
bash: cd/root/: Permission denied
andre@ubuntu1:/root$ 

andre@ubuntu1:/root$ cd ..
andre@ubuntu1:/$ ll
andre@ubuntu1:/$ su
Password: 
root@ubuntu1:/# cd /root/

授可执行权限于root
root@ubuntu1:~# chmod +x shell.sh
#who命令是。。
andre@ubuntu2:~$ who
andre    tty1         2020-04-30 12:19
andre    pts/0        2020-04-30 14:41 (192.168.100.101)
andre    pts/1        2020-04-30 15:53 (192.168.100.101)
```



